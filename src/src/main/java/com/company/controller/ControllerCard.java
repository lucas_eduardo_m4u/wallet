package com.company.controller;


import com.company.exception.*;
import com.company.model.Card;
import com.company.model.dto.RegisterCardResponseDTO;
import com.company.model.dto.ShowCardsResponseDTO;
import com.company.service.IRegisterCardService;
import com.company.service.IShowCardsService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@Controller("/cards")
public class ControllerCard {

    @Inject
    private IRegisterCardService registerCardService;

    @Inject
    private IShowCardsService showCardsService;

    @Post("/{cpf}")
    @Consumes(MediaType.APPLICATION_JSON)
    public HttpResponse<RegisterCardResponseDTO> registerCard(@PathVariable String cpf, @Valid @Body Card card) {

        try {
            return HttpResponse.created(new RegisterCardResponseDTO(
                    registerCardService.execute(cpf, card)));

        } catch (ClientCPFNotExistException exception) {
            return HttpResponse.notFound(new RegisterCardResponseDTO(exception.getResponse()));

        } catch (CardAlreadyExistException exception) {
            return HttpResponse.badRequest(new RegisterCardResponseDTO(exception.getResponse()));

        } catch (InfoCardInvalidException exception) {
            return HttpResponse.badRequest(new RegisterCardResponseDTO(exception.getResponse()));

        } catch (Exception exception) {
            return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new RegisterCardResponseDTO(
                            new ResponseError(
                                    "SERVER_ERROR", "Deu probleminha aqui que não sei oq é nao...")));
        }
    }

    @Get("/{cpf}")
    public HttpResponse<ShowCardsResponseDTO> showCards(@PathVariable String cpf) {

        try {
            List<Card> cardList = showCardsService.execute(cpf);
            return HttpResponse.ok(new ShowCardsResponseDTO(cardList));

        } catch (CardListIsEmptyException exception) {
            return HttpResponse.ok(new ShowCardsResponseDTO(exception.getResponse()));

        } catch (ClientCPFInvalidException exception) {
            return HttpResponse.badRequest(new ShowCardsResponseDTO(exception.getResponse()));

        } catch (ClientCPFNotExistException exception) {
            return HttpResponse.notFound(new ShowCardsResponseDTO(exception.getResponse()));

        } catch (Exception exception) {
            return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new ShowCardsResponseDTO(
                            new ResponseError(
                                    "SERVER_ERROR", "Deu probleminha aqui que não sei oq é nao..." )));
        }
    }
}
