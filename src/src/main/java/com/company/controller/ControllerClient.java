package com.company.controller;

import com.company.exception.ClientCPFAlreadyExistException;
import com.company.exception.ClientCPFInvalidException;
import com.company.model.dto.ClientDTO;
import com.company.service.IRegisterClientService;
import com.company.model.Client;
import com.company.service.util.ClientDTOConverter;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;

import javax.inject.Inject;
import javax.validation.Valid;

@Controller("/client")
public class ControllerClient {

    @Inject
    private IRegisterClientService IRegisterClientService;

    @Post
    @Consumes(MediaType.APPLICATION_JSON)
    public HttpResponse<?> registerClient(@Valid @Body ClientDTO clientDTO) {

        Client client = ClientDTOConverter.fromDTO(clientDTO);

        try {
            ClientDTO clientDTOResponse = ClientDTOConverter.toDTO(IRegisterClientService.execute(client));
            return HttpResponse.created(clientDTOResponse);

        } catch (ClientCPFAlreadyExistException exception) {
            return HttpResponse.badRequest(exception.getResponse());

        } catch (ClientCPFInvalidException exception){
            return HttpResponse.badRequest(exception.getResponse());
        }
    }
}
