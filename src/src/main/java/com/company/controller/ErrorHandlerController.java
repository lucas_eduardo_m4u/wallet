package com.company.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.web.router.exceptions.RoutingException;

import javax.validation.ConstraintViolationException;

@Controller
public class ErrorHandlerController {

    @Error(exception = NullPointerException.class, global = true)
    public HttpResponse<?> onNullPointerException(NullPointerException exception) {
        return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Error(exception = ArrayIndexOutOfBoundsException.class, global = true)
    public HttpResponse<?> onArrayIndexOutOfBoundsException(ArrayIndexOutOfBoundsException exception) {
        return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Error(exception = RoutingException.class, global = true)
    public HttpResponse<?> onRoutingException(RoutingException exception) {
        return HttpResponse.status(HttpStatus.BAD_REQUEST);
    }

    @Error(exception = ConstraintViolationException.class, global = true)
    public HttpResponse<?> onConstraintViolationException(ConstraintViolationException exception) {
        ResponseError response = new ResponseError("INVALID_SYNTAX", "A requisição está com a sintaxe errada ou com valores inválidos.");
        return HttpResponse.badRequest(response);
    }

    @Error(exception = Exception.class, global = true)
    public HttpResponse<?> onException(Exception exception) {
        return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}