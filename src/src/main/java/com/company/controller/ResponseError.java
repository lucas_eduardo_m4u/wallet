package com.company.controller;

public class ResponseError {

    private String code;
    private String message;

    public ResponseError(String code, String message){
        this.code = code;
        this.message = message;
    }

    public ResponseError() {
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return message;
    }
}
