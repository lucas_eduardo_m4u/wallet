package com.company.exception;

import com.company.controller.ResponseError;

public class CardAlreadyExistException extends Exception{

    private ResponseError response;

    public CardAlreadyExistException(String message) {
        super(message);
        response = new ResponseError("CARD_ALREADY_EXIST", message);
    }

    public ResponseError getResponse() {
        return response;
    }
}
