package com.company.exception;

import com.company.controller.ResponseError;

public class CardListIsEmptyException extends Exception{

    private ResponseError response;

    public CardListIsEmptyException(String message) {
        super(message);
        response = new ResponseError("EMPTY_LIST", message);

    }

    public ResponseError getResponse() {
        return response;
    }
}

