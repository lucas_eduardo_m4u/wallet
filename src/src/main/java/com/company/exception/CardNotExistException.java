package com.company.exception;

import com.company.controller.ResponseError;

public class CardNotExistException extends Exception{

    private ResponseError response;

    public CardNotExistException(String message) {
        super(message);
        response = new ResponseError("NONEXISTENT_CARD", message);
    }

    public ResponseError getResponse() {
        return response;
    }
}
