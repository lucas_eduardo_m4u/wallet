package com.company.exception;

import com.company.controller.ResponseError;

public class ClientCPFAlreadyExistException extends Exception{

    private ResponseError response;

    public ClientCPFAlreadyExistException(String message) {
        super(message);
        response = new ResponseError("CPF_ALREADY_EXIST", message);
    }

    public ResponseError getResponse() {
        return response;
    }
}
