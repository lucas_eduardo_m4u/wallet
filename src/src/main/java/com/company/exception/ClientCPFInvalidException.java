package com.company.exception;

import com.company.controller.ResponseError;

public class ClientCPFInvalidException extends Exception {

    private ResponseError response;

    public ClientCPFInvalidException(String message) {
        super(message);
        response = new ResponseError("INVALID_CPF", message);
    }

    public ResponseError getResponse() {
        return response;
    }
}
