package com.company.exception;

import com.company.controller.ResponseError;

public class ClientCPFNotExistException extends Exception{

    private ResponseError response;

    public ClientCPFNotExistException(String message) {
        super(message);
        response = new ResponseError("NONEXISTENT_CPF", message);
    }

    public ResponseError getResponse() {
        return response;
    }
}
