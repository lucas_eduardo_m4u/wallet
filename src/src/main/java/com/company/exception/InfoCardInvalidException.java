package com.company.exception;

import com.company.controller.ResponseError;

public class InfoCardInvalidException extends Exception{

    private ResponseError response;

    public InfoCardInvalidException(String message) {
        super(message);
        response = new ResponseError("INVALID_INFO_CARD", message);
    }

    public ResponseError getResponse() {
        return response;
    }
}
