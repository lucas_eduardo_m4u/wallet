package com.company.model;

import io.micronaut.core.annotation.Introspected;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Introspected
@Entity
@Table(name = "card")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @NotEmpty
    @Column(name = "num_card")
    private String numCard;

    @NotNull
    @NotEmpty
    @Column(name = "data_venc")
    private String dataVenc;


    public Card(String numCard, String dataVenc) {
        this.numCard = numCard;
        this.dataVenc = dataVenc;
    }

    public Card() {
    }

    public String getNumCard() {
        return numCard;
    }

    public String getDataVenc() {
        return dataVenc;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return numCard.equals(card.numCard) && dataVenc.equals(card.dataVenc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numCard, dataVenc);
    }

    @Override
    public String toString() {
        return "Card{" +
                "numCard='" + numCard + '\'' +
                ", dataVenc='" + dataVenc + '\'' +
                '}';
    }
}
