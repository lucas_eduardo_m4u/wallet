package com.company.model.dto;

import com.company.model.Card;
import io.micronaut.core.annotation.Introspected;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;

@Introspected
public class ClientDTO {

    @NotNull @NotEmpty @Size(min = 11, max = 11)
    private String cpf;

    @NotNull @NotEmpty
    private String name;

    private ArrayList<Card> cards = new ArrayList<>();

    public ClientDTO(String cpf, String name) {
        this.cpf = cpf;
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

}
