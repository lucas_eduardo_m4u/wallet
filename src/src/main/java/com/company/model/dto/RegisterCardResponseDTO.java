package com.company.model.dto;

import com.company.controller.ResponseError;
import com.company.model.Card;

public class RegisterCardResponseDTO {

    private Card card;
    private ResponseError error;

    public RegisterCardResponseDTO() {}

    public RegisterCardResponseDTO(ResponseError error) {
        this.error = error;
    }

    public RegisterCardResponseDTO(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public ResponseError getError() {
        return error;
    }
}
