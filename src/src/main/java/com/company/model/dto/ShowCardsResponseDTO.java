package com.company.model.dto;

import com.company.controller.ResponseError;
import com.company.model.Card;

import java.util.Collections;
import java.util.List;

public class ShowCardsResponseDTO {

    private List<Card> cards;
    private ResponseError responseError;

    public ShowCardsResponseDTO(List<Card> cards) {
        this.cards = cards;
        this.responseError = new ResponseError(null, null);
    }

    public ShowCardsResponseDTO(ResponseError responseError) {
        this.responseError = responseError;
        this.cards = Collections.emptyList();
    }

    public ShowCardsResponseDTO() {}

    public List<Card> getCards() {
        return cards;
    }

    public ResponseError getResponseError() {
        return responseError;
    }
}
