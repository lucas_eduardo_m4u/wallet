package com.company.repository;

import com.company.model.Card;
import com.company.model.Client;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

@Singleton
public class CardRepository implements ICardRepository {

    @Inject
    private ICardRepositoryJpa cardRepository;

    @Inject
    private IClientRepositoryJpa clientRepositoryJpa;

    @Inject
    private IClientRepository clientRepository;

    @Override
    @Transactional
    public Card createCard(Integer clientId, Card card) {
        Client client = clientRepository.getClient(clientId);
        client.getCards().add(card);
        return cardRepository.save(card);
    }

    @Override
    @Transactional
    public List<Card> getCards(Integer clientId) {
         List<Card> cardList = clientRepository.getClient(clientId).getCards();
        return cardList;
    }

    @Override
    @Transactional
    public boolean existsByNumCard(Integer clientId, String numCard) {
        return clientRepositoryJpa.existsByIdAndCardsNumCardEqual(clientId, numCard);
    }
}
