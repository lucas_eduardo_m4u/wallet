package com.company.repository;

import com.company.model.Client;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Singleton
public class ClientRepository implements IClientRepository {

    private IClientRepositoryJpa clientRepository;

    public ClientRepository(IClientRepositoryJpa clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client createClient(Client client) {
        return clientRepository.save(client);
    }

    @Override
    @Transactional
    public List<Client> getClients() {
       return clientRepository.findAll();

    }

    @Override
    @Transactional
    public Client getClient(Integer id) {
        Optional<Client> client = clientRepository.findById(id);

        return client.get();
    }
}
