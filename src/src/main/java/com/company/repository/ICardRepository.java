package com.company.repository;

import com.company.model.Card;

import java.util.List;

public interface ICardRepository {

    Card createCard(Integer id, Card card);

    List<Card> getCards(Integer clientId);

    public boolean existsByNumCard(Integer clientId, String numCard);
}
