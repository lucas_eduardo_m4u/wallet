package com.company.repository;

import com.company.model.Card;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface ICardRepositoryJpa extends JpaRepository<Card, Integer> {

    List<Card> findByNumCardEqual(String numCard);
    boolean existsByNumCardEqual(String numCard);
}
