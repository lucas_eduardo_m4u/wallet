package com.company.repository;

import com.company.model.Client;

import java.util.List;

public interface IClientRepository {

    Client createClient(Client client);

    List<Client> getClients();

    Client getClient(Integer id);
}
