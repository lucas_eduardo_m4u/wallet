package com.company.repository;

import com.company.model.Client;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

@Repository
public interface IClientRepositoryJpa extends JpaRepository<Client, Integer> {

    boolean existsByIdAndCardsNumCardEqual(Integer clientId, String numCard);

}
