package com.company.service;

import com.company.exception.CardAlreadyExistException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;

public interface IRegisterCardService {
    Card execute(String cpf, Card card)
        throws ClientCPFNotExistException, CardAlreadyExistException, InfoCardInvalidException;
}
