package com.company.service;

import com.company.exception.ClientCPFAlreadyExistException;
import com.company.exception.ClientCPFInvalidException;
import com.company.model.Client;

public interface IRegisterClientService {
    Client execute(Client client)
        throws ClientCPFInvalidException, ClientCPFAlreadyExistException;
}
