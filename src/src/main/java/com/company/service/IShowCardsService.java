package com.company.service;

import com.company.exception.CardListIsEmptyException;
import com.company.exception.ClientCPFInvalidException;
import com.company.exception.ClientCPFNotExistException;
import com.company.model.Card;

import java.util.List;

public interface IShowCardsService {
    List<Card> execute(String cpf)
        throws ClientCPFNotExistException, CardListIsEmptyException, ClientCPFInvalidException;
}
