package com.company.service;

import com.company.exception.CardAlreadyExistException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;
import com.company.repository.ICardRepository;
import com.company.service.util.IDataValidationService;

import javax.inject.Singleton;

@Singleton
public class RegisterCardService implements IRegisterCardService {

    private ICardRepository dataBase;

    private IDataValidationService validation;

    public RegisterCardService(ICardRepository dataBase, IDataValidationService validation) {
        this.dataBase = dataBase;
        this.validation = validation;
    }

    @Override
    public Card execute(String cpf, Card card) throws ClientCPFNotExistException, CardAlreadyExistException, InfoCardInvalidException {

        Integer clientId = validation.searchClient(cpf);

        validation.numCardValidator(card.getNumCard());
        validation.vencCardValidator(card.getDataVenc());

        if (dataBase.existsByNumCard(clientId, card.getNumCard())) {
            throw new CardAlreadyExistException("Este cartão já existe!");
        }
        return dataBase.createCard(clientId, card);
    }
}
