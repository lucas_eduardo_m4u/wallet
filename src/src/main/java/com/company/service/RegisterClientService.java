package com.company.service;

import com.company.exception.ClientCPFAlreadyExistException;
import com.company.exception.ClientCPFInvalidException;
import com.company.model.Client;
import com.company.repository.IClientRepository;
import com.company.service.util.IDataValidationService;

import javax.inject.Singleton;

@Singleton
public class RegisterClientService implements IRegisterClientService {

    private IClientRepository dataBase;

    private IDataValidationService validation;

    public RegisterClientService(IDataValidationService validation, IClientRepository dataBase) {
        this.dataBase = dataBase;
        this.validation = validation;
    }

    @Override
    public Client execute(Client client) throws ClientCPFInvalidException, ClientCPFAlreadyExistException {
        validation.cpfValidator(client.getCpf());

        for (Client clientIterator : dataBase.getClients()) {
            if (client.getCpf().equals(clientIterator.getCpf())){
                throw new ClientCPFAlreadyExistException("Este cpf já existe no sistema");
            }
        }

        return dataBase.createClient(client);
    }
}
