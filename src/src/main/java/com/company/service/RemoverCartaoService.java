package com.company.service;

import com.company.repository.ClientRepository;
import com.company.repository.IClientRepository;
import com.company.service.util.DataValidationService;
import com.company.service.util.IDataValidationService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RemoverCartaoService {

    @Inject
    private IClientRepository dataBase;
    @Inject
    private IDataValidationService validation;


    public String execute() {

        return "Cartão removido com sucesso! ";
    };
}
