package com.company.service;


import com.company.exception.CardListIsEmptyException;
import com.company.exception.ClientCPFInvalidException;
import com.company.exception.ClientCPFNotExistException;
import com.company.model.Card;
import com.company.repository.ICardRepository;
import com.company.repository.IClientRepository;
import com.company.service.util.IDataValidationService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class ShowCardsService implements IShowCardsService {

    private IDataValidationService validation;

    private IClientRepository clientRepository;

    @Inject
    private ICardRepository cardRepository;

    public ShowCardsService(IClientRepository clientRepository, IDataValidationService validation) {
        this.clientRepository = clientRepository;
        this.validation = validation;
    }

    @Override
    public List<Card> execute(String cpf) throws ClientCPFNotExistException, CardListIsEmptyException, ClientCPFInvalidException {

        validation.cpfValidator(cpf);
        Integer clientId = validation.searchClient(cpf);

        List<Card> cardList;
        cardList = cardRepository.getCards(clientId);

        validation.hasCards(cardList);

        return cardList;
    }
}
