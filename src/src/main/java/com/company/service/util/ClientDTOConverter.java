package com.company.service.util;

import com.company.model.Client;
import com.company.model.dto.ClientDTO;

import javax.inject.Singleton;

@Singleton
public class ClientDTOConverter {

    public static Client fromDTO(ClientDTO clientDTO){
        return new Client(clientDTO.getCpf(), clientDTO.getName());
    };

    public static ClientDTO toDTO(Client client){
        return new ClientDTO(client.getCpf(), client.getName());
    }
}
