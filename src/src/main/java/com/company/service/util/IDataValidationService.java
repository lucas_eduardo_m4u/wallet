package com.company.service.util;

import com.company.exception.CardListIsEmptyException;
import com.company.exception.ClientCPFInvalidException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;

import java.util.ArrayList;
import java.util.List;

public interface IDataValidationService {
    
    void cpfValidator(String cpf) throws ClientCPFInvalidException;

    void numCardValidator(String numCard) throws InfoCardInvalidException;

    void vencCardValidator(String vencCard) throws InfoCardInvalidException;

    Integer searchClient(String cpf) throws ClientCPFNotExistException;

    boolean isExistCard(Integer clientId, String numCard);

    void hasCards(List<Card> cardList) throws CardListIsEmptyException;

}
