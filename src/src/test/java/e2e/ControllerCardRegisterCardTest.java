package e2e;

import com.company.exception.ClientCPFNotExistException;
import com.company.model.Card;
import com.company.model.dto.RegisterCardResponseDTO;
import com.github.tomakehurst.wiremock.WireMockServer;
import io.micronaut.http.HttpMethod;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientException;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Optional;

@MicronautTest
public class ControllerCardRegisterCardTest {

    private final WireMockServer wireMockServer = new WireMockServer();

    @Inject
    @Client("/cards")
    RxHttpClient httpClient;

    @BeforeEach
    public void setUp() {
        this.wireMockServer.start();
    }

    @AfterEach
    public void shutDown() {
        this.wireMockServer.stop();
    }

    @Test
    void deveRetornar201EOCartaoCadastradoQuandoAChamadaParaCadastrarCartaoServiceTiverSucesso() {
        Card cardForCreate = new Card("4444444444444443", "05/2023");

        HttpRequest<Object> request = HttpRequest.create(HttpMethod.POST, "/12345678909")
                .body(cardForCreate);

        HttpResponse<RegisterCardResponseDTO> response = httpClient.toBlocking()
                .exchange(request, RegisterCardResponseDTO.class);

        Assertions.assertEquals(HttpStatus.CREATED, response.getStatus());
        Assertions.assertEquals(cardForCreate, response.body().getCard());
    }

    @Test
    void deveRetornar404AoLancarClientCpfNotExistException() {
        Card cardForCreate = new Card("4444444444444443", "05/2023");

        HttpRequest<Object> request = HttpRequest.create(HttpMethod.POST, "/16878950740")
                .body(cardForCreate);

        HttpClientResponseException httpClientResponse = Assertions.assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking()
                .exchange(request, RegisterCardResponseDTO.class));

//        httpClientResponse
//                .getResponse()
//                .getBody(RegisterCardResponseDTO.class)
//                .ifPresentOrElse(response -> {
//                    Assertions.assertEquals(HttpStatus.NOT_FOUND, httpClientResponseException.getStatus());
//                    Assertions.assertEquals("NONEXISTENT_CPF", response.getError().getCode());
//                }, () -> Assertions.fail());

        Optional<RegisterCardResponseDTO> responseBody = httpClientResponse
                .getResponse()
                .getBody(RegisterCardResponseDTO.class);

        Assertions.assertTrue(responseBody.isPresent());
        Assertions.assertEquals(HttpStatus.NOT_FOUND, httpClientResponse.getStatus());
        Assertions.assertEquals("NONEXISTENT_CPF", responseBody.get().getError().getCode());
    }

    @Test
    void deveRetornar400AoLancarCardAlreadyExistException() {
        HttpRequest<Object> request = HttpRequest.create(HttpMethod.POST, "/12345678909")
                .body(new Card("1111222233334444", "10/2022")); // client e cards pré-setados no ClientRepository

        HttpClientResponseException httpClientResponse =
                Assertions.assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking()
                        .exchange(request, RegisterCardResponseDTO.class));

        Optional<RegisterCardResponseDTO> responseBody = httpClientResponse
                .getResponse()
                .getBody(RegisterCardResponseDTO.class);

        Assertions.assertTrue(responseBody.isPresent());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, httpClientResponse.getStatus());
        Assertions.assertEquals("CARD_ALREADY_EXIST", responseBody.get().getError().getCode());
    }

    @Test
    void deveRetornar400AoLancarInfoCardInvalidException() {
        Card cardForCreate = new Card("123412341234123", "05/23");//numCard e dataVenc faltando caracteres

        HttpRequest<Object> request = HttpRequest.create(HttpMethod.POST, "/12345678909")
                .body(cardForCreate);

        HttpClientResponseException httpClientResponse =
                Assertions.assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking()
                        .exchange(request, RegisterCardResponseDTO.class));

        Optional<RegisterCardResponseDTO> responseBody = httpClientResponse
                .getResponse()
                .getBody(RegisterCardResponseDTO.class);

        Assertions.assertTrue(responseBody.isPresent());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, httpClientResponse.getStatus());
        Assertions.assertEquals("INVALID_INFO_CARD", responseBody.get().getError().getCode());
    }
}
