package e2e;

import com.company.model.Card;
import com.company.model.dto.ShowCardsResponseDTO;
import com.github.tomakehurst.wiremock.WireMockServer;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@MicronautTest
public class ControllerCardShowCardsTest {

    private final WireMockServer wireMockServer = new WireMockServer();

    @Inject
    @Client("/cards")
    RxHttpClient httpClient;

    @BeforeEach
    public void setUp() {
        this.wireMockServer.start();
    }

    @AfterEach
    public void shutDown() {
        this.wireMockServer.stop();
    }

    @Test
    void deveRetornar200EAListaDeCartoesQuandoAChamadaParaMostrarCartoesServiceTiverSucesso() {

        List<Card> cardList = new ArrayList<>();
        cardList.add(new Card("1111222233334444", "10/2022"));
        cardList.add(new Card("9999888877774445", "15/3000"));

        HttpRequest<?> request = HttpRequest.GET("/12345678909");

        HttpResponse<ShowCardsResponseDTO> response = httpClient.toBlocking()
                .exchange(request, ShowCardsResponseDTO.class);

        Assertions.assertEquals(cardList, response.body().getCards());
        Assertions.assertEquals(HttpStatus.OK, response.getStatus());
    }

    @Test
    void deveRetornar200AoLancarCardListIsEmptyException() {

        HttpRequest<Object> request = HttpRequest.GET("/12345678908");

            HttpResponse<ShowCardsResponseDTO> response = httpClient.toBlocking()
                    .exchange(request, ShowCardsResponseDTO.class);

            Assertions.assertEquals(HttpStatus.OK, response.getStatus());
            Assertions.assertEquals("EMPTY_LIST", response.body().getResponseError().getCode());
    }

    @Test
    void deveRetornar400AoLancarClientCpfInvalidException() {

        HttpRequest<Object> request = HttpRequest.GET("/1234567890"); //cpf faltando  caractere

            HttpClientResponseException httpClientResponse = Assertions.assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking()
                    .exchange(request, ShowCardsResponseDTO.class));

            Optional<ShowCardsResponseDTO> responseBody = httpClientResponse
                    .getResponse()
                    .getBody(ShowCardsResponseDTO.class);

            Assertions.assertTrue(responseBody.isPresent());
            Assertions.assertEquals(HttpStatus.BAD_REQUEST, httpClientResponse.getStatus());
            Assertions.assertEquals("INVALID_CPF",  responseBody.get().getResponseError().getCode());
    }

    @Test
    void deveRetornar404AoLancarClientCpfNotExistException() {
        HttpRequest<?> request = HttpRequest.GET("/16878950740");

        HttpClientResponseException httpClientResponse = Assertions.assertThrows(HttpClientResponseException.class, () -> httpClient.toBlocking()
                .exchange(request, ShowCardsResponseDTO.class));

        Optional<ShowCardsResponseDTO> responseBody = httpClientResponse
                .getResponse()
                .getBody(ShowCardsResponseDTO.class);

            Assertions.assertEquals(HttpStatus.NOT_FOUND, httpClientResponse.getStatus());
            Assertions.assertEquals("NONEXISTENT_CPF", responseBody.get().getResponseError().getCode());
    }
}
