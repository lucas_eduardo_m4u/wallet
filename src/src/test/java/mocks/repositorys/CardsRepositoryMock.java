package mocks.repositorys;

import com.company.model.Card;
import com.company.model.Client;
import com.company.repository.ICardRepository;
import com.company.repository.IClientRepository;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class CardsRepositoryMock implements ICardRepository {

    private Card card;
    private List<Card> dbCards = new ArrayList<>();

    @Override
    public Card createCard(Integer id, Card card) {
        this.dbCards.add(card);
        return card;
    }

    @Override
    public List<Card> getCards(Integer clientId) {
        return dbCards;
    }

    @Override
    public boolean existsByNumCard(Integer clientId, String numCard) {
        return false;
    }

}