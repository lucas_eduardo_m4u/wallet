package mocks.repositorys;

import com.company.exception.ClientCPFNotExistException;
import com.company.model.Card;
import com.company.model.Client;
import com.company.repository.IClientRepository;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class ClientsRepositoryMock implements IClientRepository {

    private Client client;
    private List<Client> dbClients = new ArrayList<>();

    @Override
    public Client createClient(Client client) {
        dbClients.add(client);
        return this.client = client;
    }

    @Override
    public List<Client> getClients() {
        return dbClients;
    }

    @Override
    public Client getClient(Integer id) {
        for (int i = 0; i <= dbClients.size(); i++) {
            if (id.equals(dbClients.get(i).getId())){
                return dbClients.get(i);
            }
        }
        return null;
    }
}