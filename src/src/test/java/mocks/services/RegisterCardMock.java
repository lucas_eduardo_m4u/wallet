package mocks.services;

import com.company.exception.CardAlreadyExistException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;
import com.company.repository.ICardRepository;
import com.company.service.IRegisterCardService;
import com.company.service.util.IDataValidationService;
import mocks.repositorys.CardsRepositoryMock;
import mocks.validation.DataValidationServiceMock;

import javax.inject.Singleton;

@Singleton
public class RegisterCardMock implements IRegisterCardService {

    private CardsRepositoryMock dataBase;

    private DataValidationServiceMock validation;

    public RegisterCardMock(CardsRepositoryMock dataBase, DataValidationServiceMock validation) {
        this.dataBase = dataBase;
        this.validation = validation;
    }

    @Override
    public Card execute(String cpf, Card card) throws ClientCPFNotExistException, CardAlreadyExistException, InfoCardInvalidException {

        Integer clientId = validation.searchClient(cpf);

        validation.numCardValidator(card.getNumCard());
        validation.vencCardValidator(card.getDataVenc());

        if (validation.isExistCard(clientId, card.getNumCard())) {
            throw new CardAlreadyExistException("Este cartão já existe!");
        }
        return dataBase.createCard(clientId, card);
    }
}
