package mocks.services;

import com.company.exception.ClientCPFAlreadyExistException;
import com.company.exception.ClientCPFInvalidException;
import com.company.model.Client;
import com.company.service.IRegisterClientService;
import com.company.service.util.IDataValidationService;
import mocks.repositorys.ClientsRepositoryMock;

import javax.inject.Singleton;

@Singleton
public class RegisterClientMock implements IRegisterClientService {

    private ClientsRepositoryMock dataBase;

    private IDataValidationService validation;

    public RegisterClientMock(IDataValidationService validation, ClientsRepositoryMock dataBase) {
        this.dataBase = dataBase;
        this.validation = validation;
    }

    @Override
    public Client execute(Client client) throws ClientCPFInvalidException, ClientCPFAlreadyExistException {
        validation.cpfValidator(client.getCpf());

        for (Client clientIterator : dataBase.getClients()) {
            if (client.getCpf().equals(clientIterator.getCpf())){
                throw new ClientCPFAlreadyExistException("Este cpf já existe no sistema");
            }
        }

        return dataBase.createClient(client);
    }
}
