package mocks.services;


import com.company.exception.CardListIsEmptyException;
import com.company.exception.ClientCPFInvalidException;
import com.company.exception.ClientCPFNotExistException;
import com.company.model.Card;
import com.company.repository.IClientRepository;
import com.company.service.IShowCardsService;
import com.company.service.util.IDataValidationService;
import mocks.repositorys.CardsRepositoryMock;
import mocks.repositorys.ClientsRepositoryMock;
import mocks.validation.DataValidationServiceMock;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ShowCardsMock implements IShowCardsService {

    private DataValidationServiceMock validation;

    private ClientsRepositoryMock clientRepository;

    private CardsRepositoryMock cardsRepository;

    public ShowCardsMock(ClientsRepositoryMock clientRepository, DataValidationServiceMock validation, CardsRepositoryMock cardsRepository) {
        this.clientRepository = clientRepository;
        this.validation = validation;
        this.cardsRepository = cardsRepository;
    }

    @Override
    public List<Card> execute(String cpf) throws ClientCPFNotExistException, CardListIsEmptyException, ClientCPFInvalidException {

        validation.cpfValidator(cpf);
        Integer clientId = validation.searchClient(cpf);

        List<Card> cardList;
        cardList = cardsRepository.getCards(clientId);

        validation.hasCards(cardList);

        return cardList;
    }
}
