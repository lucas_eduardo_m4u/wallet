package mocks.validation;


import com.company.exception.CardListIsEmptyException;
import com.company.exception.ClientCPFInvalidException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;
import com.company.model.Client;
import com.company.repository.ICardRepository;
import com.company.repository.IClientRepository;
import com.company.service.util.IDataValidationService;
import mocks.repositorys.CardsRepositoryMock;
import mocks.repositorys.ClientsRepositoryMock;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.YearMonth;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class DataValidationServiceMock implements IDataValidationService {

    private ClientsRepositoryMock clientRepository;

    private CardsRepositoryMock cardRepository;

    public DataValidationServiceMock(ClientsRepositoryMock clientRepository, CardsRepositoryMock cardRepository) {
        this.clientRepository = clientRepository;
        this.cardRepository = cardRepository;
    }

    @Override
    public void cpfValidator(String cpf) throws ClientCPFInvalidException {
        if (cpf.length() != 11) {
            throw new ClientCPFInvalidException("O CPF informado é inválido!");
        }
    }

    @Override
    public void numCardValidator(String numCard) throws InfoCardInvalidException {
        if (numCard.length() != 16) {
            throw new InfoCardInvalidException("O número do cartão informado é inválido!");
        }
    }

    @Override
    public void vencCardValidator(String vencCard) throws InfoCardInvalidException {
        if (vencCard.length() != 7) {
            throw new InfoCardInvalidException("O formato de mês/ano inserido é inválido! Formato correto: (xx/xxxx)!");
        }

        String[] mesEAno = vencCard.split("/");
        int vencMonth = Integer.parseInt(mesEAno[0]);
        int vencYear = Integer.parseInt(mesEAno[1]);

        int currentYear = YearMonth.now().getYear();
        int currentMonth = YearMonth.now().getMonthValue();

        if (vencMonth > 12 || vencMonth < 1) {
            throw new InfoCardInvalidException("O mês de vencimento informado é inválido!");
        }

        if (vencYear == currentYear && vencMonth < currentMonth) {
            throw new InfoCardInvalidException("Este cartão está vencido!");
        }

        if (vencYear < currentYear) {
            throw new InfoCardInvalidException("Este cartão está vencido!");
        }

    }

    @Override
    public Integer searchClient(String cpf) throws ClientCPFNotExistException {
        List<Client> clientList = clientRepository.getClients();
        for (int i = 0; i < clientList.size(); i++) {
            if (cpf.equals(clientList.get(i).getCpf())) {
                return clientList.get(i).getId();
            }
        }
        throw new ClientCPFNotExistException("O CPF informado não existe no nosso sistema!");
    }

    @Override
    public boolean isExistCard(Integer clientId, String numCard) {
        List<Card> clientCards = clientRepository.getClient(clientId).getCards();
        for (int i = 0; i < clientCards.size(); i++) {
            if (numCard.equals(clientCards.get(i).getNumCard())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void hasCards(List<Card> cardList) throws CardListIsEmptyException {
        if (cardList.size() == 0) {
            throw new CardListIsEmptyException("Não há nenhum cartão registrado!");
        }
    }
}
