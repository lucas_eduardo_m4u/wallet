package service;

import com.company.exception.CardAlreadyExistException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;
import com.company.model.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import mocks.repositorys.CardsRepositoryMock;
import mocks.repositorys.ClientsRepositoryMock;
import mocks.services.RegisterCardMock;
import mocks.validation.DataValidationServiceMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

@MicronautTest
public class RegisterCardServiceTest {

    @Mock
    RegisterCardMock service;

    @Mock
    DataValidationServiceMock validation;

    @Mock
    CardsRepositoryMock cardsRepository;

    @Mock
    ClientsRepositoryMock clientsRepository;

    @BeforeEach
    private void beforeEach() {
        MockitoAnnotations.openMocks(this);
        this.cardsRepository = new CardsRepositoryMock();
        this.service = new RegisterCardMock(cardsRepository, validation);
    }

    @Test
    public void oRetornoDaServiceDeveSerIgualAoCartaoRecebidoPeloParametro() throws ClientCPFNotExistException, CardAlreadyExistException, InfoCardInvalidException {
        Card cardForCreate = new Card("4444000012344567", "02/2022");
        clientsRepository.createClient(new Client("22222222222", "Teste"));

        Card registeredCard = service.execute("22222222222", cardForCreate);

        Assertions.assertEquals(cardForCreate, registeredCard);
        Mockito.verify(validation).searchClient("22222222222");
        Mockito.verify(validation).numCardValidator(cardForCreate.getNumCard());
        Mockito.verify(validation).vencCardValidator(cardForCreate.getDataVenc());
        Mockito.verify(validation).isExistCard(0, cardForCreate.getNumCard());
    }

    @Test
    public void deveLancarCardAlreadyExistQuandoCartaoJaExiste() throws ClientCPFNotExistException, CardAlreadyExistException, InfoCardInvalidException {
        Card cardForCreate = new Card("1234123445674567", "02/2022");
        clientsRepository.createClient(new Client("33333333333", "Teste"));
        cardsRepository.createCard(null, cardForCreate);

        Mockito.when(validation.isExistCard(0, cardForCreate.getNumCard())).thenReturn(true);

        Assertions.assertThrows(CardAlreadyExistException.class, () -> service.execute("33333333333", cardForCreate));

    }
}
