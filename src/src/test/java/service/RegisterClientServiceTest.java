package service;

import com.company.exception.ClientCPFAlreadyExistException;
import com.company.exception.ClientCPFInvalidException;
import com.company.model.Client;
import com.company.service.RegisterClientService;
import com.company.service.util.DataValidationService;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import mocks.repositorys.ClientsRepositoryMock;
import mocks.services.RegisterClientMock;
import mocks.validation.DataValidationServiceMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

@MicronautTest
public class RegisterClientServiceTest {

    @Mock
    private RegisterClientMock service;

    @Inject
    @Mock
    private DataValidationServiceMock validation;

    @Mock
    private ClientsRepositoryMock dataBase;

    @BeforeEach
    public void beforeEach(){
        MockitoAnnotations.openMocks(this);
        this.dataBase = new ClientsRepositoryMock();
        this.service = new RegisterClientMock(validation, dataBase);
    }

    @Test
    public void oClienteRetornadoDeveSerOMesmoRecebidoNoParametro() throws ClientCPFAlreadyExistException, ClientCPFInvalidException {

        Client clientForRegister = new Client("12345678910", "Alfredo");

        Client RegisteredClient = service.execute(clientForRegister);

        Assertions.assertEquals(clientForRegister, RegisteredClient);
        Mockito.verify(validation).cpfValidator(clientForRegister.getCpf());
    }

    @Test
    public void deveLançarClientCPFAlreadyExistExceptionQuandoClienteJaExisteNaBaseDeDados() throws ClientCPFAlreadyExistException {

        Client clientForRegister = new Client("11145666698", "Rodorrrfo");

        dataBase.createClient(clientForRegister);

        Assertions.assertThrows(ClientCPFAlreadyExistException.class, () -> service.execute(clientForRegister));
    }

}
