package service;

import com.company.model.Card;
import com.company.model.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import mocks.repositorys.CardsRepositoryMock;
import mocks.repositorys.ClientsRepositoryMock;
import mocks.services.ShowCardsMock;
import mocks.validation.DataValidationServiceMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Any;

import javax.inject.Inject;
import java.util.ArrayList;

@MicronautTest
public class ShowCardsServiceTest {

    @Mock
    private ShowCardsMock service;

    @Mock
    @Inject
    private DataValidationServiceMock validation;

    @Mock
    private ClientsRepositoryMock clientsRepository;

    @Mock
    @Inject
    private CardsRepositoryMock cardsRepository;

    @BeforeEach
    private void beforeEach() {
        MockitoAnnotations.openMocks(this);
        this.clientsRepository = new ClientsRepositoryMock();
        this.service = new ShowCardsMock(clientsRepository, validation, cardsRepository);
    }

    @Test
    public void deveRetornarAListaDeCartoesDeUmClientEspecifico() throws Exception {
        Client newClient = new Client("44444444444", "Teste");
        Card newCard = new Card("1234123412341234", "07/2025");

        clientsRepository.createClient(newClient);
        cardsRepository.createCard(null, newCard);

        Assertions.assertEquals(cardsRepository.getCards(null), service.execute("44444444444"));
    }
}
