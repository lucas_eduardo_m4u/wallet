package service.util;

import com.company.exception.CardListIsEmptyException;
import com.company.exception.ClientCPFInvalidException;
import com.company.exception.ClientCPFNotExistException;
import com.company.exception.InfoCardInvalidException;
import com.company.model.Card;
import com.company.model.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import mocks.repositorys.CardsRepositoryMock;
import mocks.repositorys.ClientsRepositoryMock;
import mocks.validation.DataValidationServiceMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.YearMonth;
import java.util.List;
import java.util.Random;

@MicronautTest
public class DataValidationServiceTest {

    private DataValidationServiceMock validation;

    private ClientsRepositoryMock clientsRepository;

    private CardsRepositoryMock cardsRepository;

    @BeforeEach
    private void beforeEach() {
        this.clientsRepository = new ClientsRepositoryMock();
        this.validation = new DataValidationServiceMock(clientsRepository, cardsRepository);
        this.cardsRepository = new CardsRepositoryMock();
    }

    @Test
    public void deveLancarClientCpfInvalidExceptionQuandoTamanhoDoCpfForMAiorQue11Caracteres() throws ClientCPFInvalidException {
        String cpfMaiorQue11 = "123123123123";

        Assertions.assertThrows(ClientCPFInvalidException.class, () -> validation.cpfValidator(cpfMaiorQue11));
    }

    @Test
    public void deveLancarClientCpfInvalidExceptionQuandoTamanhoDoCpfForMenorQue11Caracteres() throws ClientCPFInvalidException {
        String cpfMenorQue11 = "1231231231";

        Assertions.assertThrows(ClientCPFInvalidException.class, () -> validation.cpfValidator(cpfMenorQue11));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionQuandoONumeroDoCartaoForMaiorQue16Caracteres() {
        String numCardMaiorQue16 = "12341234123412342";

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.numCardValidator(numCardMaiorQue16));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionQuandoONumeroDoCartaoForMenorQue16Caracteres() {
        String numCardMenorQue16 = "123412347123412";

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.numCardValidator(numCardMenorQue16));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionOTamanhoDeCaracteresForMenorQue7() {
        String vencCard = "4/5000";

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.vencCardValidator(vencCard));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionQuandoOMesForMaiorQue12() {
        String vencCard = "13/5000";

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.vencCardValidator(vencCard));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionQuandoOMesForMenorQue1() {
        String vencCard = "00/5000";

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.vencCardValidator(vencCard));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionQuandoOAnoForIgualOuMaiorQueOAnoAtualEOMesForInferiorAoMesAtual() {
        String currentYear = Integer.toString(YearMonth.now().getYear());
        String currentMonthMinus1 = Integer.toString(YearMonth.now().getMonthValue() - 1);

        String vencCard;
        if (currentMonthMinus1.length() < 2) {
            vencCard = "0" + currentMonthMinus1 + "/" + currentYear;
        } else {
            vencCard = currentMonthMinus1 + "/" + currentYear;
        }

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.vencCardValidator(vencCard));
    }

    @Test
    public void deveLancarInfoCardInvalidExceptionQuandoOAnoDeVencimentoForMenorQueOAnoAtual() {
        String currentYearMinus1 = Integer.toString(YearMonth.now().getYear() - 1);
        String currentMonth = Integer.toString(YearMonth.now().getMonthValue());

        String vencCard = currentYearMinus1 + "/" + currentMonth;

        Assertions.assertThrows(InfoCardInvalidException.class, () -> validation.vencCardValidator(vencCard));
    }


    @Test
    public void deveRetornarOIndiceDoClienteNoBancoQuandoEleExiste() throws ClientCPFNotExistException {
        Client client = new Client("66666666666", "teste");
        client.setId(new Random().nextInt(25));

        clientsRepository.createClient(client);

        Assertions.assertEquals(client.getId(), validation.searchClient(client.getCpf()));
    }

    @Test
    public void deveRetornarClientCpfNotExistExceptionQuandoClienteNaoExiste() throws ClientCPFNotExistException {
        Client client = new Client("77777777777", "teste");

        Assertions.assertThrows(ClientCPFNotExistException.class, () -> validation.searchClient(client.getCpf()));
    }

    @Test
    public void deveRetornarTrueQuandoExisteOCartaoAssociadoAoCliente() {
        Client client = new Client("88888888888", "Teste");
        client.setId(2);
        clientsRepository.createClient(client);
        Card card = new Card("1234123412341234", "09/2029");

        clientsRepository.getClient(2).getCards().add(card);

        Assertions.assertTrue(validation.isExistCard(2, card.getNumCard()));
    }

    @Test
    public void deveRetornarFalseQuandoNaoExisteOCartaoAssociadoAoCliente() {
        Client client = new Client("99999999999", "Teste");
        client.setId(3);
        clientsRepository.createClient(client);
        Card card = new Card("1234123412341234", "09/2029");

        Assertions.assertFalse(validation.isExistCard(client.getId(), card.getNumCard()));
    }

    @Test
    public void deveLancarCardListEmptyExceptionQuandoAListaDeCartoesEstaVazia() {
        Client client = new Client("99999999990", "Teste");
        client.setId(10);
        clientsRepository.createClient(client);

        List<Card> cardList = clientsRepository.getClient(10).getCards();

        Assertions.assertThrows(CardListIsEmptyException.class, () -> validation.hasCards(cardList));
    }
}
